# NBP currency rates

Helper library to get official NBP currency exchange rates for any given day. 

### Installation
Run composer to fetch project from bitbucket repository:
```sh
$ composer require arteq/nbp-currency
```

### Usage
```php
<?php
require __DIR__ . '/vendor/autoload.php';

$nbp = new \ArteQ\NBP\CurrencyJson('2018-05-05');
$info = $nbp->getInfo();

echo "Kurs USD: ".$nbp->getCurrency('USD')."\n";
echo "Kurs EUR: ".$nbp->getCurrency('EUR')."\n";
echo "Tabela: ".$info['numer_tabeli']."\n";
echo "Data publikacji: ".$info['data_publikacji']."\n";
```

### Usage (old XML)
Old XML API is deprecated, requests can be blocked by NBP - should not be used any more.

```php
<?php
require __DIR__ . '/vendor/autoload.php';

$nbp = new \ArteQ\NBP\CurrencyXml('2018-05-05');
$info = $nbp->getInfo();

echo "Kurs USD: ".$nbp->getCurrency('USD')."\n";
echo "Kurs EUR: ".$nbp->getCurrency('EUR')."\n";
echo "Tabela: ".$info['numer_tabeli']."\n";
echo "Data publikacji: ".$info['data_publikacji']."\n";
```