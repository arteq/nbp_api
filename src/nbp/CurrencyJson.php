<?php
/**
 * 
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2022. All rights reserved.
 */

namespace ArteQ\NBP;

class CurrencyJson
{
	/**
	 * How many days we can go back to find valid date
	 */
	const MAX_TRIES = 5;

	/**
	 * NBP API endpoint address
	 */ 
	const URL_NBP = 'https://api.nbp.pl/api';

	/**
	 * NBP exchange table: [A|B|C]
	 */
	private $table = 'A';

	/**
	 * Curl handler
	 * @var CurlHandle
	 */ 
	private $curl;

	/**
	 * Date in format YYYY-MM-DD
	 * @var string
	 */  
	private $date = '';

	/**
	 * Currency result
	 * @var array
	 */ 
	private $result;

	/**
	 * Table result
	 * @var array
	 */ 
	private $info;

	/* ====================================================================== */

	/**
	 * Create object for given date
	 *
	 * @param string $date date in format YYYY-MM-DD
	 * @param array $options additional options
	 */
	public function __construct($date, $options = [])
	{
		// check for valid date date format
		if (!preg_match('/^\d{4}-\d{2}-\d{2}$/', $date))
			throw new \Exception('Nieprawidłowa data, podaj datę w formacie: YYYY-MM-DD');
		$this->date = $date;

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_HTTPHEADER, ['Accept: application/json']);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		// check if we need to connect via proxy
		if (isset($options['proxy']))
			curl_setopt($curl, CURLOPT_PROXY, $options['proxy']);

		if (isset($options['table']))
			$this->table = $options['table'];
		
		$this->curl = $curl;	
	}

	/* ====================================================================== */

	/**
	 * Get information about table number & publish date from XML file
	 *
	 * @return array
	 */
	public function getInfo()
	{	
		if (empty($this->info))
			$this->fetchTableResult();

		$info =  [
			'numer_tabeli' => $this->info['no'],
			'data_publikacji' => $this->info['effectiveDate'],
		];

		return $info;
	}

	/* ====================================================================== */

	/**
	 * Get exchange rate for a given currency symbol
	 *
	 * @param  string $symbol
	 * @return float
	 */
	public function getCurrency($symbol = 'EUR')
	{
		if (empty($this->result))
			$this->fetchCurrencyResult($symbol);

		return $this->result['mid'];
	}

	/**
	 * Fetch table for given currency symbol and date. 
	 * Try previous days in case table for given one was not publised.
	 * 
	 * @param string $symbol
	 * @return array
	 */ 
	private function fetchCurrencyResult($symbol)
	{
		$tries = 0;
		while ($tries < self::MAX_TRIES) {
			$tries++;
			
			curl_setopt($this->curl, CURLOPT_URL, sprintf('%s/exchangerates/rates/%s/%s/%s', 
				self::URL_NBP, $this->table, $symbol, $this->date));

			$out = curl_exec($this->curl);
			
			// check response header
			$curlInfo = curl_getinfo($this->curl);
			
			// table not found, go back one day and try again
			if ($curlInfo['http_code'] === 404) {
				$this->date = date('Y-m-d', strtotime($this->date.' -1 day'));
				continue;
			}

			if ($curlInfo['http_code'] !== 200)
				throw new \Exception($out);

			$out = json_decode((string)$out, true);

			// check for error
			if ($out === false)
				throw new \Exception(curl_error($this->curl));

			$this->result = $out['rates'][0];
			return $this->result;
		}

		throw new \Exception("Nie udało się pobrać kursu z tabeli");
	}

	/**
	 * Fetch whole table for given date. 
	 * Try previous days in case table for given one was not publised.
	 * 
	 * @return array
	 */ 
	private function fetchTableResult()
	{
		$tries = 0;
		while ($tries < self::MAX_TRIES) {
			$tries++;
			
			curl_setopt($this->curl, CURLOPT_URL, sprintf('%s/exchangerates/tables/%s/%s', 
				self::URL_NBP, $this->table, $this->date));

			$out = curl_exec($this->curl);
			
			// check response header
			$curlInfo = curl_getinfo($this->curl);
			
			// table not found, go back one day and try again
			if ($curlInfo['http_code'] === 404) {
				$this->date = date('Y-m-d', strtotime($this->date.' -1 day'));
				continue;
			}

			if ($curlInfo['http_code'] !== 200)
				throw new \Exception($out);

			$out = json_decode((string)$out, true);

			// check for error
			if ($out === false)
				throw new \Exception(curl_error($this->curl));

			$this->info = $out[0];
			return $this->info;
		}

		throw new \Exception("Nie udało się pobrać tabeli");
	}
}