<?php
/**
 * Get official NBP (Narodowy Bank Polski) currency exchange rate for a given
 * date and currency symbol. 
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2017. All rights reserved.
 */

namespace ArteQ\NBP;

class CurrencyXml
{
	/**
	 * How many days we can go back to find valid date
	 */
	const MAX_TRIES = 5;

	/**
	 * NBP URL with xml files
	 */ 
	const URL_NBP = 'https://www.nbp.pl/kursy/xml/';

	/**
	 * Name of file holding list of XML files for each day
	 * @var string
	 */ 
	private $urlList = 'dir.txt';

	/**
	 * Name of XML file with currencies for a given day
	 * @var string
	 */ 
	private $urlFile = '';

	/**
	 * Downloaded content of single XML file for a given day
	 * @var string
	 */ 
	private $xmlFile = '';

	/**
	 * Downloaded content of $urlList file
	 * @var string
	 */ 
	private $txtList = '';

	/**
	 * Parsed date in format YYMMDD
	 * @var string
	 */  
	private $date = '';

	/**
	 * Counter of how many days we moved back
	 * @var int
	 */ 
	private $tries = 0;

	/* ====================================================================== */

	/**
	 * Create object for given date
	 *
	 * @param string $date date in format YYYY-MM-DD
	 * @param array $options additional options
	 */
	public function __construct($date, $options = array())
	{
		$this->date = substr($date, 2, 2).substr($date, 5, 2).substr($date, 8, 2);

		// check for valid date date format
		if (!preg_match('/^\d{6}$/', $this->date))
			throw new \Exception('Nieprawidłowa data, podaj datę w formacie: YYYY-MM-DD');

		// set list file for previous years
		if (substr($date, 0, 4) < date('Y'))
			$this->urlList = 'dir'.substr($date, 0, 4).'.txt';

		// check if we need to connect via proxy
		if (getenv('PROXY') !== null)
			$proxy = getenv('PROXY');
		elseif (getenv('HTTP_PROXY') !== null)
			$proxy = getenv('HTTP_PROXY');
		elseif (isset($options['proxy']))
			$proxy = $options['proxy'];

		if (!empty($proxy))
		{
			$contextProxy = array(
				'https' => array(
					'proxy' => 'tcp://'.$proxy,
					'request_fulluri' => true,
				)
			);
			stream_context_set_default($contextProxy);
		}
	}

	/* ====================================================================== */

	/**
	 * Get information about table number & publish date from XML file
	 *
	 * @return array
	 */
	public function getInfo()
	{		
		if (empty($this->xmlFile))
			$this->getFile();

		$xml = new \SimpleXMLElement($this->xmlFile);
		$info =  array(
			'numer_tabeli' => (string)$xml->numer_tabeli,
			'data_publikacji' => (string)$xml->data_publikacji
		);

		return $info;
	}

	/* ====================================================================== */

	/**
	 * Get exchange rate for a given currency symbol
	 *
	 * @param  string $symbol
	 * @return float
	 */
	public function getCurrency($symbol = 'EUR')
	{
		if (empty($this->xmlFile))
			$this->getFile();

		$xml = new \SimpleXMLElement($this->xmlFile);

		foreach ($xml->pozycja as $p)
		{
			$kod = (string)$p->kod_waluty;

			if ($kod == $symbol)
			{
				$kurs = (float)preg_replace('/,/', '.', $p->kurs_sredni) * (float)$p->przelicznik;
				return $kurs;
			}
		}

		throw new \Exception("Nie odnaleziono waluty o symbolu: ".$symbol);
	}

	/* ====================================================================== */

	/**
	 * Get list of NBP exchange rates tables, set proper table for a given date
	 * 
	 * @return string name of XML file with currencies table
	 */
	private function getList()
	{
		while ($this->tries < self::MAX_TRIES)
		{
			$this->tries++;

			// download from NBP
			$this->txtList = file_get_contents(self::URL_NBP.$this->urlList);
			if ($this->txtList == FALSE)
				throw new \Exception("Nie udało się pobrać listy arkuszy ze strony NBP");

			$pattern = '/^a[0-9]{3}z'.$this->date.'/m';
			if (preg_match($pattern, $this->txtList, $matches))
			{
				if (count($matches) != 1)
				{
					throw new \Exception("Błędna liczba znalezionych plików z kursami");
				}
				else
				{
					$this->urlFile = $matches[0];
					return $this->urlFile;
				}
			}
			
			// go back one day, ex in case of weekend or holiday
			$this->dayBack();
		}

		throw new \Exception("Nie udało się znaleźć tabeli dla tego dnia");
	}

	/* ====================================================================== */

	/**
	 * Load content of right exchange rate table
	 */
	private function getFile()
	{
		if (empty($this->urlFile))
			$this->getList();

		// download from NBP
		$this->xmlFile = file_get_contents(self::URL_NBP.$this->urlFile.'.xml');
		if ($this->xmlFile == FALSE)
			throw new \Exception("Nie udało się pobrać pliku z kursami ze strony NBP");
	}

	/* ====================================================================== */
	
	/**
	 * Go one day back (rates are not published on weekends & holidays)
	 */
	private function dayBack()
	{
		$newDate = '20'.$this->date;
		$newDate = substr_replace($newDate, '-', 4, 0);
		$newDate = substr_replace($newDate, '-', 7, 0);
		$newDate = date('ymd', strtotime('-1 day', strtotime($newDate)) );

		$this->date = $newDate;
		unset($this->urlFile);
		unset($this->xmlFile);
		unset($this->txtList);
	}
}