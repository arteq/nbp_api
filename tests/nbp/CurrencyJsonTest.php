<?php
/**
 * 
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2022. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\NBP\CurrencyJson;

class CurrencyJsonTest extends TestCase
{
	public function setUp()
	{
		date_default_timezone_set('Europe/Warsaw');
	}

	public function testCanBeCreatedFromValidDate()
	{
		$this->assertInstanceOf(
			CurrencyJson::class,
			new CurrencyJson('2022-05-05')
		);
	}

	public function testCantBeCreatedFromInvalidData()
	{
		$this->expectException(\Exception::class);

		new CurrencyJson('2012-AA-01');
	}

	public function testCanGetValidCurrencyUSD()
	{
		$currency = new CurrencyJson('2017-05-05');
		$rate = $currency->getCurrency('USD');

		$this->assertEquals($rate, 3.849);
	}

	public function testCanGetValidCurrencyInfoUSD()
	{
		$currency = new CurrencyJson('2017-05-05');
		$info = $currency->getInfo();

		$this->assertEquals($info['numer_tabeli'], '086/A/NBP/2017');
		$this->assertEquals($info['data_publikacji'], '2017-05-05');
	}

	public function testCanGetValidCurrencyEUR()
	{
		$currency = new CurrencyJson('2017-05-05');
		$rate = $currency->getCurrency('EUR');

		$this->assertEquals($rate, 4.2176);
	}

	public function testCantGetInvalidCurrency()
	{
		$this->expectException(\Exception::class);

		$currency = new CurrencyJson('2017-05-05');
		$currency->getCurrency('XYZ');
	}

	public function testCantGetFromFuture()
	{
		$this->expectException(\Exception::class);

		$currency = new CurrencyJson('2500-01-01');
		$currency->getCurrency('EUR');
	}

	public function testCanGetBeforeWeekend()
	{
		// holiday = no exchange rate from that day, 
		$currency = new CurrencyJson('2017-05-01');
		$info = $currency->getInfo();

		// 04-29 & 04-30 = weekend no rate here either
		$this->assertEquals($info['data_publikacji'], '2017-04-28');
	}

}